/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.algorithmes.generateurdecles;

import java.util.Random;
import moteurcryptov2.donnees.cles.Cle;
import moteurcryptov2.donnees.cles.CleInteger;
import moteurcryptov2.donnees.cles.Cles;

/**
 * Classe qui genere les clés pour le codage de Cesar
 * @author Alexandre
 * @version 8.2
 */
public class GenerateurDeClesCesar implements GenerateurDeCles{
    
    @Override
    public Cles genererClePublique() {
        return null;
    }

    @Override
    public Cles genererClePrivee() {
        Cles c = new Cles();
        
        Random r = new Random();
        r.setSeed(this.hashCode());
        c.addCle("cleCesar", new CleInteger(r.nextInt(25)+1));
        
        return c;
    }
    
}
