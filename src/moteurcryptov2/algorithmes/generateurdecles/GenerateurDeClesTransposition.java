/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.algorithmes.generateurdecles;

import java.security.SecureRandom;
import moteurcryptov2.donnees.cles.CleString;
import moteurcryptov2.donnees.cles.Cles;

/**
 * Classe qui genere les clés pour le codage par transposition
 * @author Alexandre
 * @version 8.2
 */
public class GenerateurDeClesTransposition implements GenerateurDeCles{

    private int tailleCle;

    /**
     * Constructeur de GenerateurDeClesTransposition
     * @param tailleCle la taille de la cle pour le codage
     */
    public GenerateurDeClesTransposition(int tailleCle) {
        this.tailleCle = tailleCle;
    }
    
    @Override
    public Cles genererClePublique() {
        return null;
    }

    @Override
    public Cles genererClePrivee() {
        
        Cles c = new Cles();
        StringBuilder res = new StringBuilder();
        SecureRandom r = new SecureRandom();
        int temp = 0;
        
        for(int i=0;i<this.tailleCle;i++){
            do {            
                temp = r.nextInt(57)+65;
            } while (!((temp >= 65 && temp <= 90) || (temp >= 97 && temp <= 122)));
            
            res.append((char)temp);
        }
        
        c.addCle("cleTransposition", new CleString(res.toString()));
        return c;
        
    }
    
}
