/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.algorithmes.generateurdecles;

import java.util.Random;
import moteurcryptov2.donnees.cles.CleInteger;
import moteurcryptov2.donnees.cles.Cles;

/**
 * Classe qui génére les clé de RLE
 * @author Alexandre
 * @version 8.2
 */
public class GenerateurDeClesRLE implements GenerateurDeCles{

    private int tailleMaxCle;

    /**
     * Contructeur du generateur
     * @param tailleCle taille Maximal de la clée
     */
    public GenerateurDeClesRLE(int tailleCle) {
        this.tailleMaxCle = tailleCle;
    }
    
    
    
    @Override
    public Cles genererClePublique() {
        Cles c = new Cles();
        Random r = new Random();
        
        // On genere une cle entiere aléatoire entre 2 et la taille maximal
        c.addCle("cleRLE", new CleInteger(r.nextInt(tailleMaxCle-1)+2));
        
        return c;
    }

    @Override
    public Cles genererClePrivee() {
        return null;
    }
    
}
