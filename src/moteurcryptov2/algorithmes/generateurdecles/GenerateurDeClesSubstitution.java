/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.algorithmes.generateurdecles;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import moteurcryptov2.donnees.cles.Cle;
import moteurcryptov2.donnees.cles.CleString;
import moteurcryptov2.donnees.cles.Cles;

/**
 * Classe qui génère la cle de substitution
 * @author Alexandre
 * @version 8.2
 */
public class GenerateurDeClesSubstitution implements GenerateurDeCles{

    @Override
    public Cles genererClePublique() {
        return null;
    }

    @Override
    public Cles genererClePrivee() {
        Cles c = new Cles();
        
        ArrayList<Character> alphabet = new ArrayList<>(); 
        for(int i=65;i<91;i++){
            alphabet.add((char)(i));
        }
        StringBuilder sb = new StringBuilder();
        Collections.shuffle(alphabet);
        for(Character car : alphabet){
            sb.append(car);
        }
        
        c.addCle("cleSubstitution", new CleString(sb.toString()));
        return c;
    }
    
}
