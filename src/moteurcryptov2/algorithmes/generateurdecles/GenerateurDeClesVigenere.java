/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.algorithmes.generateurdecles;

import java.security.SecureRandom;
import moteurcryptov2.donnees.cles.Cle;
import moteurcryptov2.donnees.cles.CleString;
import moteurcryptov2.donnees.cles.Cles;

/**
 * Classe qui génère la cle de Vigenere
 * @author Alexandre
 * @version 8.2
 */
public class GenerateurDeClesVigenere implements GenerateurDeCles{

    private int tailleCle;

    /**
     * Constructeur de GenerateurDeClesVigenere
     * @param tailleCle la taille de la clé génerer
     */
    public GenerateurDeClesVigenere(int tailleCle) {
        this.tailleCle = tailleCle;
    }
    
    @Override
    public Cles genererClePublique() {
        return null;
    }

    @Override
    public Cles genererClePrivee() {
        
        Cles c = new Cles();
        StringBuilder sb = new StringBuilder();
        
        SecureRandom r = new SecureRandom();
        int temp = 0;
        
        for(int i=0;i<this.tailleCle;i++){
            temp = r.nextInt(25)+65;
            sb.append((char)temp);
        }
        
        c.addCle("cleVigenere", new CleString(sb.toString()));      
        return c;            
    }
    
}
