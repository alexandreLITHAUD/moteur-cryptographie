/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.algorithmes.generateurdecles;

import moteurcryptov2.donnees.cles.Cles;

/**
 * Inferface d'un generateur de clé
 * @author Alexandre
 * @version 8.2
 */
public interface GenerateurDeCles {
    
    /**
     * Fonction qui genere des clé publiques
     * @return une liste de cles publiques
     */
    public Cles genererClePublique();
    
    /**
     * Fonction qui genere des clé privées
     * @return une liste de cles privées
     */
    public Cles genererClePrivee();
    
}
