package moteurcryptov2.algorithmes.chiffrement;

import java.util.Dictionary;
import moteurcryptov2.algorithmes.chiffrement.huffman.Noeud;
import moteurcryptov2.donnees.cles.Cles;
import moteurcryptov2.donnees.messages.Message;
import moteurcryptov2.exceptions.ExceptionCryptographie;
import java.util.HashMap;
import java.util.PriorityQueue;
import moteurcryptov2.algorithmes.chiffrement.huffman.ComparateurNoeuds;
import moteurcryptov2.donnees.messages.MessageString;
import moteurcryptov2.entities.Univers;

/**
 * Description de la classe
 * @author Matthieu
 */
public class AlgorithmeHuffman implements Algorithme {

    private HashMap<Character,String> dico;
    
    @Override
    public String getNom() {
        return "Huffman";
    }
    
    //Etape 1 - Comptage des caractères ----------------------------------------
    private HashMap<Character,Integer> compterCaracteres(String message) {
        HashMap<Character, Integer> map = new HashMap<>();
        
        for(Character c :message.toCharArray()){
            Integer temp = map.get(c);
            if(temp == null){
                map.put(c, 1);
            }else{
                map.put(c, map.get(c)+1);
            }
        }          
        return map;
    }
    //--------------------------------------------------------------------------
    
    
    
    
    //Etape 2 - Création de l'arbre --------------------------------------------
    private PriorityQueue<Noeud> creationListeNoeuds(HashMap<Character,Integer> mapComptageCaractere) {
        
        PriorityQueue<Noeud> priorityMap = new PriorityQueue<>(new ComparateurNoeuds());
        for(Character c : mapComptageCaractere.keySet()){
            Noeud n = new Noeud(c.toString(), mapComptageCaractere.get(c));
            priorityMap.add(n);
        }
        
        return priorityMap;
    }
    
    private Noeud creationArbre(HashMap<Character,Integer> mapComptageCaractere) {
        
        PriorityQueue<Noeud> priorityMap = this.creationListeNoeuds(mapComptageCaractere);
        
        while(priorityMap.size() >= 2){
            
            Noeud N1 = priorityMap.poll();
            Noeud N2 = priorityMap.poll();
            
            Noeud N = new Noeud(N1.getNom().concat(N2.getNom()), N1.getNombreOccurences()+N2.getNombreOccurences());
            N.ajouterFils(N1);
            N.ajouterFils(N2);
            
            priorityMap.add(N);
        }
        
        return priorityMap.peek();
    }
    //--------------------------------------------------------------------------
    
   
    
    
    //Etape 3 - Création du dictionnaire----------------------------------------
    private HashMap<Character,String> creationDictionnaire(Noeud racine) {
        
        HashMap<Character,String> dictionnaire = new HashMap<>();
        racine.calculCode(dictionnaire);
        
        return dictionnaire;
    }
    //--------------------------------------------------------------------------

    
    
    
    //Etape 4 - Chiffrement du message -----------------------------------------
    private String chiffrerMessage(String message,HashMap<Character,String> dictionnaire) {
        
        String res = "";
        
        for(Character c:message.toCharArray()){           
            res += dictionnaire.get(c);            
        }
              
        return res;
    }
    //--------------------------------------------------------------------------
    
    
    
    @Override
    public Message chiffrer(Message message, Cles clesPubliques, Cles clesPrivees) throws ExceptionCryptographie {
        
        String messageString = message.asString();
        
        HashMap<Character,Integer> map = this.compterCaracteres(messageString);
        
        Noeud racine = this.creationArbre(map);
        
        HashMap<Character,String> dictionnaire = this.creationDictionnaire(racine);
        this.dico = dictionnaire;
        
        String res = this.chiffrerMessage(messageString, dictionnaire);
        
        Message m = new MessageString(res);

        return m;
    }

    @Override
    public Message dechiffrer(Message message, Cles clesPubliques, Cles clesPrivees) throws ExceptionCryptographie {
        
        //IL FAUT UN MOYEN DE PRENDRE LE DICTIONNAIRE DE BASE
        
        // POUR CHAQUE VALEUR DU MESSAGE SI SA VALEUR = FEUILLE
        // ON AJOUTE LA VALEUR DE LA FEUILLE
        
        //SINON ON PREND UN ELEMENT EN PLUS ET ON RECOMMENCE
        
        
        // JE N'AI PAS TROUVE DE FACON CORRECT DE RECUPERER LE DICTIONNAIRE
        
        String messageString = message.asString();
        StringBuilder sb = new StringBuilder();
        //String res = "";
        String temp = "";
        
        for(Character c:messageString.toCharArray()){
            
            temp += c;
            if(dico.containsValue(temp)){
                sb.append(dico.get(temp));  // JE NE SAIS TROP POURQUOI IL NE PREND PAS LA VALUR DE LA CLEF
                temp = "";
            }
            
        }
        
        Message m = new MessageString(sb.toString());
        
        return m;
    }
    
    
    
}
