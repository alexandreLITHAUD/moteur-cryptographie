/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.algorithmes.chiffrement;

import moteurcryptov2.donnees.cles.Cle;
import moteurcryptov2.donnees.cles.Cles;
import moteurcryptov2.donnees.messages.Message;
import moteurcryptov2.donnees.messages.MessageString;
import moteurcryptov2.exceptions.ExceptionCryptographie;

/**
 * Classe de l'Algorthme de Vigenere
 * @author Alexandre
 * @version 8.2
 */
public class AlgorithmeVigenere implements Algorithme{

    private char[][] tableauVigenere = new char[26][26];

    /**
     * Constructeur de l'Algorithme
     * qui initialise la tableau de Vigenere
     */
    public AlgorithmeVigenere() {
        this.initTableauxVigenere();
    }
    
    @Override
    public String getNom() {
        return "AlgorithmeVigenere";
    }

    @Override
    public Message chiffrer(Message message, Cles clePubliques, Cles clePrivee) throws ExceptionCryptographie {
        
        String messageToUpperCase = message.asString().toUpperCase();
        
        Cle clePrincipale = clePrivee.getCle("cleVigenere");
        
        StringBuilder sb = new StringBuilder();
     
        do{
            sb.append(clePrincipale.asString());
        }while(sb.toString().length() < messageToUpperCase.length());
        
        String cleTotale = sb.toString();
        StringBuilder resultat = new StringBuilder();
        
        for(int i=0;i<messageToUpperCase.length();i++){
            if(messageToUpperCase.charAt(i) >= 65 && messageToUpperCase.charAt(i) <= 90){
                resultat.append(this.getVigenereAssociationChiffre(cleTotale.charAt(i), messageToUpperCase.charAt(i)));
            }
            else{
                resultat.append(messageToUpperCase.charAt(i));
            }
        }
        
        MessageString messageCodee = new MessageString(resultat.toString());
        return messageCodee;
        
    }

    @Override
    public Message dechiffrer(Message message, Cles clePubliques, Cles clePrivee) throws ExceptionCryptographie {
        
        String messageToUpperCase = message.asString().toUpperCase();
        
        Cle clePrincipale = clePrivee.getCle("cleVigenere");
        
        StringBuilder sb = new StringBuilder();
     
        do{
            sb.append(clePrincipale.asString());
        }while(sb.toString().length() < messageToUpperCase.length());
        
        String cleTotale = sb.toString();
        StringBuilder resultat = new StringBuilder();
        
        for(int i=0;i<messageToUpperCase.length();i++){
            if(messageToUpperCase.charAt(i) >= 65 && messageToUpperCase.charAt(i) <= 90){
                resultat.append(this.getVigenereAssociationDechiffre(cleTotale.charAt(i), messageToUpperCase.charAt(i)));
            }
            else{
                resultat.append(messageToUpperCase.charAt(i));
            }
        }
        
        MessageString messageDecodee = new MessageString(resultat.toString());
        return messageDecodee;
        
    }


    /**
     * Fonction qui retoune le caracère codé dans le tableaux de Vigenere
     * @param ligne le charartere de ligne
     * @param colone le chararctere de colone
     * @return le caratère code selon le tableau
     */
    private char getVigenereAssociationChiffre(char ligne, char colone) {
        int numligne = ligne - 65;
        int numColone = colone - 65;
        
        return this.tableauVigenere[numligne][numColone];
    }
    
    /**
     * Fonction qui retoune le caracère decode dans le tableaux de Vigenere
     * @param cle le caractere de la cle
     * @param message le caractere a chercher dans le tableau
     * @return le caractere decode selon le tableau
     */
    private char getVigenereAssociationDechiffre(char cle, char message) {

        int num = cle - 65;
        
        for(int i=0;i<26;i++){
            if(this.tableauVigenere[num][i] == message){
                return (char)(i+65);
            }
        }
        
        return ' ';
    }

    /**
     * Fonction qui initialise le tableau de Vegenere
     */
    private void initTableauxVigenere() {
        
        char a = 65;
        
        for(int i=0;i<26;i++){  
            a=(char) (65+i);
            for(int j=0; j<26;j++){
                this.tableauVigenere[i][j] = a;              
                a++;
                if(a>90) {
                    a=65;
                }
            }
        }
    }
    
}
