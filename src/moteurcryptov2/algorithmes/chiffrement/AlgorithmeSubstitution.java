/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.algorithmes.chiffrement;

import java.util.HashMap;
import java.util.Map;
import moteurcryptov2.donnees.cles.Cle;
import moteurcryptov2.donnees.cles.Cles;
import moteurcryptov2.donnees.messages.Message;
import moteurcryptov2.donnees.messages.MessageString;
import moteurcryptov2.exceptions.ExceptionCryptographie;

/**
 * Classe de l'Algorthme de substitution
 * @author Alexandre
 * @version 8.2
 */
public class AlgorithmeSubstitution implements Algorithme{

    private Map<Character, Character> tableauxDeSubstitution = new HashMap<>();
       
    @Override
    public String getNom() {
        return "AlgorithmeSubstitution";
    }

    @Override
    public Message chiffrer(Message message, Cles clePubliques, Cles clePrivee) throws ExceptionCryptographie {
        
        String messageUpper = message.asString().toUpperCase();
        
        Cle clePrincipal = clePrivee.getCle("cleSubstitution");
        
        for(int i=0;i<clePrincipal.asString().length();i++){
            this.tableauxDeSubstitution.put((char)(i+65),clePrincipal.asString().charAt(i));
        }
        
        StringBuilder resultat = new StringBuilder();
        
        for(int i=0;i<messageUpper.length();i++){
            if(messageUpper.charAt(i) >= 65 && messageUpper.charAt(i) <= 90){
                resultat.append(this.tableauxDeSubstitution.get(messageUpper.charAt(i)));
            }
            else{
                resultat.append(messageUpper.charAt(i));
            }
        }
        
        MessageString messageCodee = new MessageString(resultat.toString());
        return messageCodee;
        
    }

    @Override
    public Message dechiffrer(Message message, Cles clePubliques, Cles clePrivee) throws ExceptionCryptographie {
        
        String messageUpper = message.asString().toUpperCase();
        
        Cle clePrincipal = clePrivee.getCle("cleSubstitution");
        
        for(int i=0;i<clePrincipal.asString().length();i++){
            this.tableauxDeSubstitution.put(clePrincipal.asString().charAt(i),(char)(i+65));
        }
        
        StringBuilder resultat = new StringBuilder();
        
        for(int i=0;i<messageUpper.length();i++){
            if(messageUpper.charAt(i) >= 65 && messageUpper.charAt(i) <= 90){
                resultat.append(this.tableauxDeSubstitution.get(messageUpper.charAt(i)));
            }
            else{
                resultat.append(messageUpper.charAt(i));
            }
        }
        
        MessageString messageDecodee = new MessageString(resultat.toString());
        return messageDecodee;
    }
    
}
