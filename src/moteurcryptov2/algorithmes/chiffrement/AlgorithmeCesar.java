/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.algorithmes.chiffrement;

import java.util.ArrayList;
import moteurcryptov2.donnees.cles.Cles;
import moteurcryptov2.donnees.messages.Message;
import moteurcryptov2.donnees.messages.MessageASCII;
import moteurcryptov2.exceptions.ExceptionCryptographie;

/**
 * Classe de l'Algorthme de Cesar
 * @author Alexandre
 * @version 8.2
 */
public class AlgorithmeCesar implements Algorithme{

    @Override
    public String getNom() {
        return "AlgorithmeCesar";
    }

    @Override
    public Message chiffrer(Message message, Cles clePubliques, Cles clePrivee) throws ExceptionCryptographie {
        
        ArrayList<Integer> ascii = message.getListAsciiCode();
        ArrayList<Integer> res = new ArrayList<>();
        
        for(int a : ascii){
            
            if(a <= 122 && a >= 97){
                a+= clePrivee.getCle("cleCesar").asInteger();
                
                if(a > 122) {
                    int temp = a-122;
                    a = 96+temp;
                }
            }
            
            else if(a <= 90 && a >= 65){
                a+= clePrivee.getCle("cleCesar").asInteger();
                
                if(a > 90) {
                    int temp = a-90;
                    a = 65+temp;
                }
            }
            
            res.add(a);
            
        }
        
        Message me = new MessageASCII(res);
        return me;   
        
    }

    @Override
    public Message dechiffrer(Message message, Cles clePubliques, Cles clePrivee) throws ExceptionCryptographie {
        
        ArrayList<Integer> ascii = message.getListAsciiCode();
        ArrayList<Integer> res = new ArrayList<>();
        
        for(int a : ascii){
            
            if(a <= 122 && a >= 97){
                a -= clePrivee.getCle("cleCesar").asInteger();
                
                if(a < 97) {
                    int temp = 97-a;
                    a = 123-temp;
                }
            }
            
            else if(a <= 90 && a >= 65){
                a-= clePrivee.getCle("cleCesar").asInteger();
                
                if(a < 65) {
                    int temp = 65-a;
                    a = 65-temp;
                }
            }
            
            res.add(a);
            
        }
        
        Message me = new MessageASCII(res);
        return me;
    }
    
    
    
    
}
