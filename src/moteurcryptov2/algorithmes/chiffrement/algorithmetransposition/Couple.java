/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.algorithmes.chiffrement.algorithmetransposition;

/**
 * Lcasse qui définie un couple
 * @author Alexandre
 * @version 8.2
 */
public class Couple {
    
    private char caractere;
    private int position;

    /**
     * Constructeur d'un couple
     * @param caractere le caractère du couple
     * @param position la position du couple
     */
    public Couple(char caractere, int position) {
        this.caractere = caractere;
        this.position = position;
    }

    /**
     * Fonction qui retoune le caractère du couple
     * @return le caractère du Couple
     */
    public char getCaractere() {
        return caractere;
    }

    /**
     * Fonction qui définie le caractère du Couple
     * @param caractere le caractère du couple
     */
    public void setCaractere(char caractere) {
        this.caractere = caractere;
    }

    /**
     * Fonction qui retoune la position du couple
     * @return la position du Couple
     */
    public int getPosition() {
        return position;
    }

    /**
     * Fonction qui définie la position du Couple
     * @param position la position du couple
     */
    public void setPosition(int position) {
        this.position = position;
    }
    
    
}
