/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.algorithmes.chiffrement.algorithmetransposition;

import java.util.Comparator;

/**
 * Classe de Comparator du Couple
 * @author Alexandre
 * @version 8.2
 */
public class ComparateurCouple implements Comparator<Couple>{

    @Override
    public int compare(Couple o1, Couple o2) {    
        int res = 0;
        
        if(o1.getCaractere() < o2.getCaractere() || (o1.getCaractere() == o2.getCaractere() && o1.getPosition() < o2.getPosition())){
            res = -1;
        }
        else{
            res = 1;
        }
        
        return res;
    }
    
}
