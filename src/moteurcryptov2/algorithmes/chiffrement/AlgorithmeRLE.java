/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.algorithmes.chiffrement;

import moteurcryptov2.donnees.cles.Cles;
import moteurcryptov2.donnees.messages.Message;
import moteurcryptov2.donnees.messages.MessageString;
import moteurcryptov2.exceptions.ExceptionCryptographie;

/**
 * Classe qui créer l'algoritme de RLE
 * @author Alexandre
 * @version 8.2
 */
public class AlgorithmeRLE implements Algorithme{

    @Override
    public String getNom() {
        return "AlgorithmeRLE";
    }

    @Override
    public Message chiffrer(Message message, Cles clePubliques, Cles clePrivee) throws ExceptionCryptographie {
        
        String res = "";
        //On recupère le message
        String str = message.asString();
        //On recupère la clé publique RLE
        int tailleCle = clePubliques.getCle("cleRLE").asInteger();
        int count = 0;

        for (int i=0;i<str.length();i++)
        {
            count = 1;
            //On compte le nombre d'occurance succesive de chaque Charactère
            while (i+1<str.length() && str.charAt(i) == str.charAt(i + 1))
            {
                count++;
                i++;
            }
            //Si le nombre de caractère a la suite dépasse la valeur de la clé publique RLE
            if(count<tailleCle){
                res += String.valueOf(count) + str.charAt(i);
            }else{
                //On calcule le quotient pour savoir cobient d'occurance maximal il faut ajouter
                int quotient = count/tailleCle;
                //On calcule le rete de la division euclidienne
                int reste = count%tailleCle;
                //la taille de la clé (donc la taille maximal)
                int base = tailleCle;
                
                //On ajoute la valeur max autant de foit que le quotient
                for(int j =0;j<quotient;j++){
                    res += String.valueOf(base) + str.charAt(i);
                }
                //On ajoute le reste si il n'est pas vide
                if(reste != 0){
                    res += String.valueOf(reste) + str.charAt(i);
                }
                
            }
            
        }
        
        Message m = new MessageString(res);
        return m;
        
    }

    @Override
    public Message dechiffrer(Message message, Cles clePubliques, Cles clePrivee) throws ExceptionCryptographie {
        
        String res = "";
        //On recupère le message (chiffré)
        String str = message.asString();
        //On recupère la clé publique RLE
        int tailleCle = clePubliques.getCle("cleRLE").asInteger();
        
        //On separe notre message tous les deux caractères
        //Ne peux pas marcher avec k>9
        String[] array=str.split("(?<=\\G.{2})");
        
        //Je pense que pourvoir gérer k>9 il faudrait voir la chaine dans sont intégralité
        //regarder tout d'abord la fin de la chaine et regarder si plusieurs numéro ce suivent
        //si c'est le cas regarder si il est supereur a la valeur de k
        // si oui alors on decoupe
        // si non on met autant que la valeur des deux numero
        
        // Je n'ai pas le temps d'essayer ma technique j'ai du resoudre mon problème de
        // projet corrompu
        
        for(String s:array){
            //On recupère le premier caractère (l'entier)
            int number = Character.getNumericValue(s.charAt(0));
            for(int i=0;i<number;i++){
                //On ajoute la la caractère autant de fois que demandé
                res+= s.charAt(1);
            }
        }
        
        Message m = new MessageString(res);
        return m;
    }
    
}
