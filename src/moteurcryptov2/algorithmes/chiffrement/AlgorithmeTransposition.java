/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.algorithmes.chiffrement;

import java.nio.ByteBuffer;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import moteurcryptov2.algorithmes.chiffrement.algorithmetransposition.ComparateurCouple;
import moteurcryptov2.algorithmes.chiffrement.algorithmetransposition.Couple;
import moteurcryptov2.donnees.cles.Cle;
import moteurcryptov2.donnees.cles.Cles;
import moteurcryptov2.donnees.messages.Message;
import moteurcryptov2.donnees.messages.MessageString;
import moteurcryptov2.exceptions.ExceptionConversionImpossible;
import moteurcryptov2.exceptions.ExceptionCryptographie;

/**
 * Classe de l'Algorthme de transposition
 * @author Alexandre
 * @version 8.2
 */
public class AlgorithmeTransposition implements Algorithme{

    private SecureRandom generateur;
    
    @Override
    public String getNom() {
        return "AlgorithmeDeTransposition";
    }

    @Override
    public Message chiffrer(Message message, Cles clePubliques, Cles clePrivee) throws ExceptionCryptographie {
        
        StringBuilder sb = new StringBuilder();
        Cle clePrincipal = clePrivee.getCle("cleTransposition");
        
        char[][] res = this.remplirTableauChiffrement(message, clePrincipal);
        ArrayList<Integer> ordre = this.getOrdreColonne(clePrincipal);
        
        int condition = 0;
        
        if(message.asString().length()%clePrincipal.asString().length() == 0){
            
            condition = message.asString().length()/clePrincipal.asString().length();
            
        }else{
            
            condition = message.asString().length()/clePrincipal.asString().length()+1;
        }
        
        for(int i :ordre){
            for(int j=0; j<condition; j++){
                sb.append(res[j][i]);
            }
        }
        
        Message m = new MessageString(sb.toString());
        return m;
        
    }

    @Override
    public Message dechiffrer(Message message, Cles clePubliques, Cles clePrivee) throws ExceptionCryptographie {
        
        StringBuilder sb = new StringBuilder();
        Cle clePrincipal = clePrivee.getCle("cleTransposition");
        
        int condition = 0;
        
        if(message.asString().length()%clePrincipal.asString().length() == 0){
            
            condition = message.asString().length()/clePrincipal.asString().length();
            
        }else{
            
            condition = message.asString().length()/clePrincipal.asString().length()+1;
        }
        
        char[][] res = new char[condition][clePrincipal.asString().length()];
        ArrayList<Integer> ordre = this.getOrdreColonne(clePrincipal);
        
        int compteur = 0;
        for(int i :ordre){
            for(int j=0; j<condition; j++){
                res[j][i] = message.asString().charAt(compteur);
                compteur++;
            }
        }    
        
        for(int i=0;i<condition;i++){
            for(int j=0;j<clePrincipal.asString().length();j++){
                sb.append(res[i][j]);
            }
        }
        
        Message m = new MessageString(sb.toString());
        return m;        
    }
    
    /**
     * Fonction qui remplir un tableaux pour le chiffrage par transposition
     * @param message le message a chiffrer
     * @param cle la clé de chiffrement
     * @return le message chiffrer
     * @throws ExceptionConversionImpossible 
     */
    private char[][] remplirTableauChiffrement(Message message,Cle cle)throws ExceptionConversionImpossible {
        
        ByteBuffer b = ByteBuffer.allocate(4);
        b.putInt((cle.asString()+message.asString()).hashCode());
        this.generateur = new SecureRandom (b.array());
        
        char[][] tab = null;
        ArrayList<Integer> ascii = message.getListAsciiCode();
        int compteur = 0;
        
        int condition = 0;
        
        if(message.asString().length()%cle.asString().length() == 0){
            
            condition = message.asString().length()/cle.asString().length();
            
        }else{
            
            condition = message.asString().length()/cle.asString().length()+1;
        }
        
        tab = new char[condition][cle.asString().length()];
        
        for(int i=0;i<condition;i++){
            for(int j=0;j<cle.asString().length();j++){
                
                if(compteur >= message.asString().length()){
                    tab[i][j] = this.bourrage();
                }
                else{
                    tab[i][j] = message.asString().charAt(compteur);
                }
                compteur++;
            }
        }
        
        
        return tab;
    }
  
    /**
     * Fonction qui creer un Char aleatoire pour le bourrage
     * @return le caractère aléatoire
     */
    private char bourrage(){
        int res = 0;
        
        do {            
            res = this.generateur.nextInt(57)+65;
        } while (!((res >= 65 && res <= 90) || (res >= 97 && res <= 122)));
        
        return (char)res;
    }
    
    /**
     * Fonction qui renvoie l'ordre de lecture de la Cle
     * @param cle la cle donc on doit trouver l'ordre
     * @return la position des couple a visiter dans l'ordre
     * @throws ExceptionConversionImpossible 
     */
    private ArrayList<Integer> getOrdreColonne(Cle cle) throws ExceptionConversionImpossible {
        
        ArrayList<Integer> res = new ArrayList<>();
        ArrayList<Couple> couples = new ArrayList<>();
        
        for(int i=0; i<cle.asString().length();i++){
            couples.add(new Couple(cle.asString().charAt(i), i));
        }
        
        Collections.sort(couples,new ComparateurCouple());
        
        for(Couple c : couples){
            res.add(c.getPosition());
        }
        
        return res;
        
    }
}
