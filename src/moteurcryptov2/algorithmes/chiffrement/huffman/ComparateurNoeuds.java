package moteurcryptov2.algorithmes.chiffrement.huffman;

import java.util.Comparator;

/**
 * Description de la classe
 * @author Matthieu
 */
public class ComparateurNoeuds implements Comparator<Noeud> {

    @Override
    public int compare(Noeud o1, Noeud o2) {
        int res = 0;
        
        if(o1.getNombreOccurences() < o2.getNombreOccurences()){
            res = -1;
        }
        else if(o1.getNombreOccurences() > o2.getNombreOccurences()){
            res = 1;
        }
        else{
            res = 0;
        }
        
        return res;      
    }

}
