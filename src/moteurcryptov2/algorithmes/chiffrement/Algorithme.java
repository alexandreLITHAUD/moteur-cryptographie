/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.algorithmes.chiffrement;

import moteurcryptov2.exceptions.ExceptionCryptographie;
import moteurcryptov2.donnees.cles.Cles;
import moteurcryptov2.donnees.messages.Message;

/**
 * Inferface d'un Algorithme
 * @author Alexandre
 * @version 8.2
 */
public interface Algorithme {
    
    /**
     * Fonction qui retourne le nom de l'Algorithme
     * @return le nom de l'algorithme
     */
    public String getNom();
    
    /**
     * Fonction qui permet de chiffre un message
     * @param message le message a chiffrer
     * @param clePubliques les clés publiques
     * @param clePrivee les clés privées
     * @return le message chiffrer
     * @throws ExceptionCryptographie 
     */
    public Message chiffrer(Message message, Cles clePubliques, Cles clePrivee) throws ExceptionCryptographie;
    
    /**
     * Fonction qui permet de dechiffre un message
     * @param message le message a dechiffrer
     * @param clePubliques les clés publiques
     * @param clePrivee les clés privées
     * @return le message dechiffrer
     * @throws ExceptionCryptographie 
     */
    public Message dechiffrer(Message message, Cles clePubliques, Cles clePrivee) throws ExceptionCryptographie;    
    
}
