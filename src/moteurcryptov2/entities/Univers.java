/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.entities;

import java.util.HashMap;
import moteurcryptov2.donnees.cles.Cles;
import moteurcryptov2.donnees.messages.Message;

/**
 * Classe de l'Univers
 * @author Alexandre
 */
public class Univers {
    
    private HashMap<String,Cles> listeClesPubliques;
    private HashMap<String,Message> listeMessagesPublics;
    private static Univers instance;
    
    /**
     * Constructeur de l'Univers
     */
    private Univers(){
        this.listeClesPubliques = new HashMap<>();
        this.listeMessagesPublics = new HashMap<>();
    }
    
    /**
     * Fonction qui permet de retourne l'intance de l'Univers
     * @return l'instance de l'Univers
     */
    public static Univers get(){
        if(Univers.instance == null){
            Univers.instance = new Univers();
        }
        return instance;
    }
    
    /**
     * Fonction qui rajoute une cle a l'Univers
     * @param identifiant l'identifiant de la cle a rajouter
     * @param cles la cle a rajouter
     */
    public static void addCles(String identifiant, Cles cles){
        get().listeClesPubliques.put(identifiant, cles);
    }
    
    /**
     * Fonction qui retoune des clés
     * @param identifiant l'identifiant de la clés
     * @return la cles
     */
    public static Cles getCles(String identifiant){
        return get().listeClesPubliques.get(identifiant);
    }
    
    /**
     * Fonction qui rajoute un message a l'Univers
     * @param identifiant l'identifiant du message a rajouter
     * @param message le message a rajouter
     */
    public static void addMessage(String identifiant, Message message){
        get().listeMessagesPublics.put(identifiant, message);
    }
    
    /**
     * Fonction qui retoune un message
     * @param identifiant l'identifiant du message
     * @return le message lier a l'identifiant
     */
    public static Message getMessage(String identifiant){
        return get().listeMessagesPublics.get(identifiant);
    }
    
    
}
