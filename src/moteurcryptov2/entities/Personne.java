/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.entities;

import moteurcryptov2.algorithmes.chiffrement.Algorithme;
import moteurcryptov2.donnees.cles.Cles;
import moteurcryptov2.donnees.messages.Message;
import moteurcryptov2.exceptions.ExceptionAlgorithmeNonDefini;
import moteurcryptov2.exceptions.ExceptionChiffrementImpossible;
import moteurcryptov2.exceptions.ExceptionCryptographie;

/**
 * Classe qui represente une personne
 * @author Alexandre
 * @version 8.2
 */
public class Personne {
    
    private String nom;
    private Algorithme algorithme;
    private Cles clesPrivees;

    /**
     * Fonction qui retourne le nom de la personne
     * @return le nom de la personne
     */
    public String getNom() {
        return nom;
    }

    /**
     * Fonction qui retoune l'Algorithme
     * @return l'Algorithme
     */
    public Algorithme getAlgorithme() {
        return algorithme;
    }

    /**
     * Fonction qui rajoute un algorithme a une personne
     * @param algorithme l'algo a rajouter a la personne
     */
    public void setAlgorithme(Algorithme algorithme) {
        this.algorithme = algorithme;
    }

    /**
     * Fonction qui retoune les clés privées
     * @return les clés privées
     */
    public Cles getClesPrivees() {
        return clesPrivees;
    }

    /**
     * Fonction qui rajoute une cles a une personne
     * @param clesPrivees 
     */
    public void setClesPrivees(Cles clesPrivees) {
        this.clesPrivees = clesPrivees;
    }

    /**
     * Constructeur d'une personne
     * @param nom le nom de la personne
     */
    public Personne(String nom) {
        this.nom = nom;
    }
    
    public Message chiffrer(Message message, Cles clePubliques) throws ExceptionCryptographie {
        
        if(this.algorithme == null){
            throw new ExceptionAlgorithmeNonDefini();
        }
        else{
            return this.algorithme.chiffrer(message, clePubliques, this.clesPrivees);
        }
        
    }
    
    public Message dechiffrer(Message message, Cles clePubliques) throws ExceptionCryptographie{
        
        if(this.algorithme == null){
            throw new ExceptionAlgorithmeNonDefini();
        }
        else{
            return this.algorithme.dechiffrer(message, clePubliques, this.clesPrivees);
        }
    } 
    
    
    
    
}
