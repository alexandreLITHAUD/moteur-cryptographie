/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.exceptions;

/**
 * Classe qui définie une ExceptionAlgorithmeNonDefini
 * @author Alexandre
 * @version 8.2
 */
public class ExceptionAlgorithmeNonDefini extends ExceptionCryptographie {

    public ExceptionAlgorithmeNonDefini() {
        super("Algo non définie", "L'algorithme n'est pas définie");
    }
    
    
}
