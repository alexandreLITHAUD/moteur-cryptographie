/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.exceptions;

/**
 * Classe qui définie une ExceptionChiffrementImpossible
 * @author Alexandre
 * @version 8.2
 */
public class ExceptionChiffrementImpossible extends ExceptionCryptographie{

    public ExceptionChiffrementImpossible() {
        super("ChiffrementImpossible", "Le chiffrement n'as pas aboutie");
    }
    
}
