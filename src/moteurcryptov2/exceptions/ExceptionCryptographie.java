/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.exceptions;

/**
 * Classe abstraite d'une Exception de Cryptographie
 * @author Alexandre
 * @version 8.2
 */
public abstract class ExceptionCryptographie extends Exception{
    
    private String nom;
    private String message;

    /**
     * Constructeur de ce type d'exception
     * @param nom nom de l'Exception
     * @param message message d'erreur
     */
    public ExceptionCryptographie(String nom, String message) {
        this.nom = nom;
        this.message = message;
    }

    /**
     * Fonction qui execute l'Exception
     */
    public void gerer(){  
        System.out.println(this.nom.toUpperCase() +" : "+ this.message);
        this.printStackTrace(System.err);
    }

}
