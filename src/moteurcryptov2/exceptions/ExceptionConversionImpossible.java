/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.exceptions;

/**
 * Classe qui définie une ExceptionConversionImpossible
 * @author Alexandre
 * @version 8.2
 */
public class ExceptionConversionImpossible extends ExceptionCryptographie{

    public ExceptionConversionImpossible() {
        super("Conversion Impossible", "La convertion n'a pas aboutie");
    }
    
}
