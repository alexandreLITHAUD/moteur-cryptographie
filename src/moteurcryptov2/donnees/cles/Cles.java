/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.donnees.cles;

import java.util.HashMap;

/**
 * Classe qui definie des Cle
 * @author Alexandre
 */
public class Cles {
    
    private HashMap<String,Cle> listeCles;
    
    /**
     * Constrcteur de la classe
     */
    public Cles(){
        this.listeCles = new HashMap<>();
    }
    
    /**
     * Fonction qui retourne la clé lier a l'identifiant
     * @param nom l'identifiant
     * @return la clé lier a l'identifiant
     */
    public Cle getCle(String nom){
        return this.listeCles.get(nom);
    }
    
    /**
     * Fonctionne qui rajoute la clé a la Map
     * @param nom l'identifiant
     * @param cle la cle a rajouter
     */
    public void addCle(String nom, Cle cle){
        this.listeCles.put(nom, cle);
    }
    
    
    
}
