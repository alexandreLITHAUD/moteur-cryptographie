/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.donnees.cles;

import java.util.logging.Level;
import java.util.logging.Logger;
import moteurcryptov2.exceptions.ExceptionChiffrementImpossible;
import moteurcryptov2.exceptions.ExceptionConversionImpossible;
import moteurcryptov2.exceptions.ExceptionCryptographie;

/**
 * Classe qui Gere une cle sous forme d'un entier
 * @author Alexandre
 * @version 8.2
 */
public class CleInteger implements Cle{
    
    private int cle;

    /**
     * Constructeur de CleInteger
     * @param cle la valeur de la cle
     */
    public CleInteger(int cle) {
        this.cle = cle;
    }

    @Override
    public String asString() {
        try {
            throw new ExceptionConversionImpossible();
        } catch (ExceptionConversionImpossible ex) {
            ex.gerer();
        }
        return String.valueOf(cle);
    }

    @Override
    public int asInteger() {
        try{
            return this.cle;
        }catch(Exception e){
            ExceptionCryptographie ex = new ExceptionConversionImpossible();
            ex.gerer();
        }
        return this.cle;
    }
    
    
}
