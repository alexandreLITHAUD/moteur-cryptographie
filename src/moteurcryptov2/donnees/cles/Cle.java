/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.donnees.cles;

/**
 * Interface qui définie une Cle
 * @author Alexandre
 */
public interface Cle {
    
    /**
     * Fonction qui retoune la clé sous forme d'une chaine de caractère
     * @return la chaine de caractère de la Cle
     */
    public String asString();
    
    /**
     * Fonction qui retoune la clé sous forme d'un entier
     * @return l'entier de la Cle
     */
    public int asInteger();
    
}
