/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.donnees.cles;

import moteurcryptov2.exceptions.ExceptionConversionImpossible;
import moteurcryptov2.exceptions.ExceptionCryptographie;

/**
 * Classe qui Gere une cle sous forme d'une chaine de caractère
 * @author Alexandre
 * @version 8.2
 */
public class CleString implements Cle{

    private String cle;

    /**
     * Constructeur de CleString
     * @param cle la valeur de la cle
     */
    public CleString(String cle) {
        this.cle = cle;
    }
    
    @Override
    public String asString() {
        
        try{
            return this.cle;
        }catch(Exception e){
            ExceptionCryptographie ex = new ExceptionConversionImpossible();
            ex.gerer();
        }
        return this.cle;
    }

    @Override
    public int asInteger() {
        
        try {
            throw new ExceptionConversionImpossible();
        } catch (ExceptionConversionImpossible ex) {
            ex.gerer();
        }
        return Integer.valueOf(cle);
    }
    
}
