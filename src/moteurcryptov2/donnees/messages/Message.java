/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.donnees.messages;

import java.util.ArrayList;
import moteurcryptov2.exceptions.ExceptionConversionImpossible;

/**
 * Interface d'un message
 * @author Alexandre
 * @version 8.2
 */
public interface Message {
    
    /**
     * Fonction qui retoune un message sous forme d'une chaine de caractère
     * @return la chaine de caractère du message
     */
    public String asString() throws ExceptionConversionImpossible;
    
    /**
     * Fonction qui retoune un message sous forme d'un entier
     * @return l'entier du message
     */
    public int asInteger() throws ExceptionConversionImpossible;
    
    /**
     * Fonction qui retoune le code ascii du message
     * @return code ascii du message
     */
    public ArrayList<Integer> getListAsciiCode();
    
}
