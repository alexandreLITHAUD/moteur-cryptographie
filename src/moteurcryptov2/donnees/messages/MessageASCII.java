/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.donnees.messages;

import java.util.ArrayList;
import moteurcryptov2.exceptions.ExceptionConversionImpossible;
import moteurcryptov2.exceptions.ExceptionCryptographie;

/**
 * Classe d'un message ASCII
 * @author Alexandre
 * @version  8.2
 */
public class MessageASCII implements Message{

    private ArrayList<Integer> ascii;

    /**
     * Constructeur de MessageASCII
     * @param ascii les valeur de la cle sous forme d'un tableau de valeur ascii
     */
    public MessageASCII(ArrayList<Integer> ascii) {
        this.ascii = ascii;
    }
    
    @Override
    public String asString() {
        
        try{
            StringBuilder sb = new StringBuilder();    
            for(int a: this.ascii){
                sb.append((char)a);
            }        
            return sb.toString();
        }catch(Exception e){
            ExceptionCryptographie ex = new ExceptionConversionImpossible();
            ex.gerer();
        }
        return null;
    }

    @Override
    public int asInteger() throws ExceptionConversionImpossible {
        throw new ExceptionConversionImpossible();
        
    }

    @Override
    public ArrayList<Integer> getListAsciiCode() {
        return this.ascii;
    }
    
}
