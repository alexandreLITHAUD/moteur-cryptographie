/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.donnees.messages;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import moteurcryptov2.exceptions.ExceptionConversionImpossible;
import moteurcryptov2.exceptions.ExceptionCryptographie;

/**
 * classe d'un message string
 * @author Alexandre
 * @version 8.2
 */
public class MessageString implements Message{

    private String message;

    public MessageString(String message) {
        this.message = message;
    }  
    
    @Override
    public String asString() {
        return this.message;
    }

    @Override
    public int asInteger() throws ExceptionConversionImpossible {
        try {
            return Integer.valueOf(message);
        } catch (Exception ex) {
            throw new ExceptionConversionImpossible();
        }
        
    }

    @Override
    public ArrayList<Integer> getListAsciiCode() {

        ArrayList<Integer> ascii = new ArrayList<>();
        for(int i = 0; i<this.message.length();i++){
            
           try{
           ascii.add((int)this.message.charAt(i));
           
           }catch(Exception ex){
               ex.printStackTrace();
           }
        }
        return ascii;
    }
    
}
