/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.protocoles;

/**
 * Interface lier a un protocole
 * @author Alexandre
 * @version 8.2
 */
public interface Protocole {
    
    /**
     * Fonction qui execute le protocole
     */
    public void executer();
    
}
