package moteurcryptov2.protocoles;

import java.util.logging.Level;
import java.util.logging.Logger;
import moteurcryptov2.algorithmes.chiffrement.AlgorithmeHuffman;
import moteurcryptov2.donnees.messages.Message;
import moteurcryptov2.donnees.messages.MessageString;
import moteurcryptov2.entities.Personne;
import moteurcryptov2.entities.Univers;
import moteurcryptov2.exceptions.ExceptionConversionImpossible;
import moteurcryptov2.exceptions.ExceptionCryptographie;
import moteurcryptov2.protocoles.Protocole;

/**
 * Description de la classe
 * @author Matthieu
 */
public class ProtocoleHuffman implements Protocole {

    @Override
    public void executer() {
        
        Personne alice = new Personne("Alice");
        
        alice.setAlgorithme(new AlgorithmeHuffman());
        
        Message m = new MessageString("EDECBEEDAEBCEDAECE");
        
        
        try {
            System.out.println(m.asString());
            Univers.addMessage("Compression Huffman", alice.chiffrer(m, null));
            String temp = Univers.getMessage("Compression Huffman").asString();
            System.out.println(temp);
            Univers.addMessage("Compression Huffman2", alice.dechiffrer(new MessageString(temp), null));
            String temp2 = Univers.getMessage("Compression Huffman2").asString();
            System.out.println(temp2);
        } catch (ExceptionCryptographie ex) {
            Logger.getLogger(ProtocoleHuffman.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
    }


}
