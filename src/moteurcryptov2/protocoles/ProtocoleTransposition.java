/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.protocoles;

import java.util.logging.Level;
import java.util.logging.Logger;
import moteurcryptov2.algorithmes.chiffrement.AlgorithmeTransposition;
import moteurcryptov2.algorithmes.generateurdecles.GenerateurDeClesTransposition;
import moteurcryptov2.donnees.cles.Cles;
import moteurcryptov2.donnees.messages.Message;
import moteurcryptov2.donnees.messages.MessageString;
import moteurcryptov2.entities.Personne;
import moteurcryptov2.entities.Univers;
import moteurcryptov2.exceptions.ExceptionCryptographie;

/**
 * Protocole du codage par transposition
 * @author Alexandre
 * @version 8.2
 */
public class ProtocoleTransposition implements Protocole{

    @Override
    public void executer() {
        
        Personne alice = new Personne("Alice");
        Personne bob = new Personne("Bob");
        
        alice.setAlgorithme(new AlgorithmeTransposition());
        bob.setAlgorithme(new AlgorithmeTransposition());
        
        GenerateurDeClesTransposition gnt = new GenerateurDeClesTransposition(10);
        Cles c = gnt.genererClePrivee();
        
        System.out.println(c.getCle("cleTransposition").asString());
        
        alice.setClesPrivees(c);
        bob.setClesPrivees(c);
        
        Message message = new MessageString("stop right there criminal scum !");
        
        try {
            Univers.addMessage("aliceEtBob", alice.chiffrer(message, null));
            System.out.println(Univers.getMessage("aliceEtBob").asString());
        } catch (ExceptionCryptographie ex) {
            Logger.getLogger(ProtocoleTransposition.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            System.out.println(bob.dechiffrer(Univers.getMessage("aliceEtBob"), null).asString());
            //bob.dechiffrer(Univers.getMessage("aliceEtBob"), null);
        } catch (ExceptionCryptographie ex) {
            Logger.getLogger(ProtocoleCesar.class.getName()).log(Level.SEVERE, null, ex);
        }    
        
    }
    
}
