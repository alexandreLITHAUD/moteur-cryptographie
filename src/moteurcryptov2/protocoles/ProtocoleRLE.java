/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moteurcryptov2.protocoles;

import java.util.logging.Level;
import java.util.logging.Logger;
import moteurcryptov2.algorithmes.chiffrement.AlgorithmeRLE;
import moteurcryptov2.algorithmes.generateurdecles.*;
import moteurcryptov2.donnees.cles.Cles;
import moteurcryptov2.donnees.messages.Message;
import moteurcryptov2.donnees.messages.MessageString;
import moteurcryptov2.entities.Personne;
import moteurcryptov2.entities.Univers;
import moteurcryptov2.exceptions.ExceptionCryptographie;

/**
 *
 * @author Alexandre
 */
public class ProtocoleRLE implements Protocole{

    @Override
    public void executer() {
        
        //On ajoute Alice
        Personne alice = new Personne("Alice");
        
        //On ajoute l'algo RLE a Alice
        alice.setAlgorithme(new AlgorithmeRLE());
        
        // On genere la clé publique de RLE
        GenerateurDeClesRLE gnr = new GenerateurDeClesRLE(9);      
        Cles c = gnr.genererClePublique();
        
        // On ajoute la clé publique a Alice
        alice.setClesPrivees(c);
        
        // On creer le message a compresser
        Message message = new MessageString("AAAAAAGGGOOOOOUUU!!222");
        
        try {
            //On affiche le mesage
            System.out.println(message.asString());
            //On chiffre le message
            Univers.addMessage("compressionAlice", alice.chiffrer(message, c));
            //On affiche le message chifré
            System.out.println(Univers.getMessage("compressionAlice").asString());
            //On dechiffre le message et on l'affiche
            System.out.println(alice.dechiffrer(Univers.getMessage("compressionAlice"), c).asString());
            //On affiche la valeur de la clé
            System.out.println(c.getCle("cleRLE").asInteger());
            //On affiche le taux de compression
            System.out.println("Taut de compression :");
            System.out.println((float)Univers.getMessage("compressionAlice").asString().length()/message.asString().length());
        } catch (ExceptionCryptographie ex) {
            Logger.getLogger(ProtocoleCesar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
